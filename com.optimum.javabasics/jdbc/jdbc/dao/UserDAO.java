package jdbc.dao;

import jdbc.pojo.User;

public interface UserDAO {

	void getUserRecord();
	void insertRecord(User refUser);
	void deleteRecord(int input);
	void updateRecord(int userID, String newPassword);
	void userLogin();
		
}
