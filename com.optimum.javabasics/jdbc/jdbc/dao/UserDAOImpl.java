package jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jdbc.pojo.User;
import utility.DBUtility;

public class UserDAOImpl implements UserDAO{

	Connection refConnection = null;
	PreparedStatement refPreparedStatement =null;
	
	@Override
	public void getUserRecord() {
		
		try {
			refConnection = DBUtility.getConnection();		
			
			String sqlQuery = "select user_id,user_password from user";
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			
			// Approach 1
			ResultSet results = refPreparedStatement.executeQuery();  

			while (results.next()) {
				int user_id = results.getInt("user_id");
				String user_password = results.getString("user_password");
				System.out.println(user_id + ", " + user_password);
			}
			
			// Approach 2
//			it record = refPreparedStatement.executeUpdate();
//			if(record>0) System.out.println("new record has been successfully inserted");
			
		} catch (SQLException e) {
			System.out.println("Exception Handled while getting all user record.");
		}
		
		finally {
			try {
				refConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			finally {
				
				System.out.println("Closing Connection..");
			}
		}
	}

	@Override
	public void insertRecord(User refUser) {
				
		try {
			refConnection = DBUtility.getConnection();
			
			String sqlQuery = "insert into user(user_id,user_password) values(?,?)";
			
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setInt(1, refUser.getUserID());
			refPreparedStatement.setString(2, refUser.getUserPassword());
			
			refPreparedStatement.execute();  
			
			// Approach 2
//			int record = refPreparedStatement.executeUpdate();
//			if(record>0) System.out.println("new record has been successfully inserted");
			
		} catch (SQLException e) {
			System.out.println("Exception Handled while insert record.");
		}
		
		finally {
			try {
				refConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			finally {
				System.out.println("Closing Connection..");
			}
		}
	} 

	@Override
	public void deleteRecord(int input) {
		try {
			refConnection = DBUtility.getConnection();		
			
			String sqlQuery = "delete from user where user_id=?";
			
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setInt(1,input);
			
			// Approach 1
			refPreparedStatement.execute();  
			
			// Approach 2
//			int record = refPreparedStatement.executeUpdate();
//			if(record>0) System.out.println("new record has been successfully inserted");
			
		} catch (SQLException e) {
			System.out.println("Exception Handled while deleting record.");
		}
		
		finally {
			try {
				refConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			finally {
				System.out.println("Closing Connection..");
			}
		}
	}

	@Override
	public void updateRecord(int userID, String newPassword) {
		try {
			refConnection = DBUtility.getConnection();	
			
			String sqlQuery = "update user set user_password=? where user_id=?";
			
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setString(1, newPassword);
			refPreparedStatement.setInt(2,userID);
			
			// Approach 1
			refPreparedStatement.execute();  
			
			// Approach 2
//			int record = refPreparedStatement.executeUpdate();
//			if(record>0) System.out.println("new record has been successfully inserted");
			
		} catch (SQLException e) {
			System.out.println("Exception Handled while deleting record.");
		}
		
		finally {
			try {
				refConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			finally {
				System.out.println("Closing Connection..");
			}
		}
	}
	
	@Override
	public void userLogin() {
		
	}
}
