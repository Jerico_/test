package jdbc.service;

import java.util.Scanner;
import jdbc.dao.UserDAO;
import jdbc.dao.UserDAOImpl;
import jdbc.pojo.User;

public class UserServiceImpl implements UserService {

	UserDAO refUserDAO;
	Scanner scannerRef;
	User refUser;
	
	@Override
	public void userInputInsertRecord() {
		scannerRef = new Scanner(System.in);
		
		System.out.println("Enter User ID : ");
		int userLoginID = scannerRef.nextInt();
		
		System.out.println("Enter User Password : ");
		String userPassword = scannerRef.next();
		
		refUser = new User();
		refUser.setUserID(userLoginID);
		refUser.setUserPassword(userPassword);
		
		refUserDAO = new UserDAOImpl();
		refUserDAO.insertRecord(refUser);
	}
	
	public void deleteRecord() {
		scannerRef = new Scanner(System.in);
		
		System.out.println("Enter User ID to delete: ");
		int input = scannerRef.nextInt();
		
		refUserDAO = new UserDAOImpl();
		refUserDAO.deleteRecord(input);
	}
	
	public void getUserRecord() {
		refUserDAO = new UserDAOImpl();
		refUserDAO.getUserRecord();
	}
	
	public void updateRecord() {
		scannerRef = new Scanner(System.in);
		System.out.println("Enter User ID to update: ");
		int userID = scannerRef.nextInt();
		
		System.out.println("Enter your new password: ");
		String newPassword = scannerRef.next();
		
		refUserDAO = new UserDAOImpl();
		refUserDAO.updateRecord(userID, newPassword);
	}

	@Override
	public void userChoice() {
		System.out.println("Enter Choice");
		scannerRef = new Scanner(System.in);
		int choice = scannerRef.nextInt();
		switch (choice) {
		case 1:
			userInputInsertRecord();
			break;
			
		case 2:
			deleteRecord();
			break;
			
		case 3:
			getUserRecord();
			break;
			
		case 4: 
			updateRecord();
			break;

		default:
			System.out.println("Option not found..");
			break;

		}
	}
}
