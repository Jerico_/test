package day9;

// BufferedReader example on Wrapper classes (Int, Double, Float, etc)
// to convert String obj to prim data type

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Example29 {
	public static void main(String[] args) throws IOException {
		BufferedReader refBufferedReader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number: ");
		//int number = refBufferedReader.read(); will give ascii. must use parseInt
		
		int number = Integer.parseInt(refBufferedReader.readLine());
		System.out.println(number);
		
		double number1 = Double.parseDouble(refBufferedReader.readLine());
		System.out.println(number1);
		
	}
}
