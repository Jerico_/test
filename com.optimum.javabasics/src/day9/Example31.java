package day9;

public class Example31 {
	public static void main(String[] args) {
		
		// creating 1D array
		int productNumber[] = {10, 20, 30};
		for (int i : productNumber) {
			System.out.println(i);
		}
		
		// declare array first then allocate memory
		int productKey[] = new int[3];
		productKey[0] = 15;
		productKey[1] = 25;
		productKey[2] = 35;
		
		for (int i : productKey) {
			System.out.println(i);
		}
	}
}
