package day9;

public class Product {
	int productID;
	String productName;

	public Product(int productID, String productName) {
		this.productID = productID;
		this.productName = productName;
	}
	
	@Override
	public String toString() {
		return productID + " " + productName;
	}
}
