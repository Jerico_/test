package day9;

import java.util.Scanner;

// do while ex

public class Example30 {
	public static void main(String[] args) {
		
		String userChoice = null;
		do {
		Scanner scannerRef = new Scanner(System.in);
		System.out.println("Enter name: ");
		
		String userName = scannerRef.next();
		System.out.println("Continue? Y/N");
		
		userChoice = scannerRef.next();
		} while (!userChoice.equals("N"));
		System.out.println("Bye!");
	}
}
