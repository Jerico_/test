package day9;

import java.util.Scanner;

public class Example32 {
	public static void main(String[] args) {
		System.out.println("How many products do you want to add to cart?");
		Scanner refScanner = new Scanner(System.in);
		
		int number = refScanner.nextInt();
		
		// create array of Product class
		Product refProduct[] = new Product[number];
		
		for (int i = 0; i < refProduct.length; i++) {
			System.out.println("Enter product ID:");
			int productID = refScanner.nextInt();
			
			System.out.println("Enter product name:");
			String productName = refScanner.next();
			refProduct[i] = new Product(productID, productName);
		}
					
		for (Product p: refProduct) {
			System.out.println(p);
		}
	}
}

