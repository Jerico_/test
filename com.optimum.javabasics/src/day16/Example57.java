package day16;

// if we have mult threads running and we need to execute any 
// particular thread on a high priority

class ThreadDemo extends Thread{
	@Override
	public void run() {
		// optional for loop, just for understanding how thread works
		for (int i=0; i<=2; i++) {
			try {
				Thread.sleep(3000); // 5s
			}
			catch (Exception e) {
				System.out.println("Exception handled");
			}
			System.out.println(i);
			// check which thread is running under main() thread
			System.out.println(currentThread().getName());
		}
	}
}

public class Example57 {
	public static void main(String[] args) {
		ThreadDemo refThreadDemo = new ThreadDemo();
		
		Thread refThread1 = new Thread(refThreadDemo);
		Thread refThread2 = new Thread(refThreadDemo);
		Thread refThread3 = new Thread(refThreadDemo);
		Thread refThread4 = new Thread(refThreadDemo);
		Thread refThread5 = new Thread(refThreadDemo);
		
		refThread1.setName("Thread 1");
		refThread2.setName("Thread 2");
		refThread3.setName("Thread 3");
		refThread4.setName("Thread 4");
		refThread5.setName("Thread 5");
		
		refThread1.start();
		refThread2.start();
		
		// to execute thread 1,2 first
		try {
			refThread2.join();
		} catch (InterruptedException e) {
			e.printStackTrace(); // or syso(e)
		}
		
		// will execute 3,4,5 after the loop ends 	
		refThread3.start();
		refThread4.start();
		refThread5.start();
	}
}
