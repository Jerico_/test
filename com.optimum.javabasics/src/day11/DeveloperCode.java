package day11;

public class DeveloperCode {
	static void getNumber() {
		try {
			System.out.println("opening files");
			int num = 10;
			System.out.println(num/0);
		} catch (Exception e) {
			System.out.println("cannot divide by 0");
		}
		
		finally {
			System.out.println("closing files");
		}
	}
}
