package day3;

class Employee{
	String employeeName = "Jerico";
	
	@Override
	public String toString() {
		return employeeName;
	}
}
public class Example10 {

	public static void main(String[] args) {
		Employee refEmployee = new Employee();
		//System.out.println(refEmployee); // returns mem address day3.Employee@4926097b
		// either System.out.println(refEmployee.employeeName) or add override then
		System.out.println(refEmployee);
	}

}
