package day3;

// encap demo

public class Example06 {

	public static void main(String[] args) {
		University refUniversity = new University();
		refUniversity.setUniversityID(1000);
		refUniversity.setUniversityName("XYZ");
		refUniversity.setUniversityLocation("ABC");
		
		System.out.println(refUniversity.getUniversityID() + " " + refUniversity.getUniversityName() + " " + refUniversity.getUniversityLocation());
	}

}
