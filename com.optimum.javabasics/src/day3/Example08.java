package day3;

// method overloading ex

class Mobile{
//	private void getFeatures(boolean wifi) {
//		System.out.println(wifi);
//	}
	
	public String getFeatures(String brandName) {
		return brandName;
	}
	
	String getFeatures(int mobileCode, String mobileLocation) {
		return mobileCode + " " + mobileLocation;
	}
	
	void getFeatures(long mobileDiscountOfferPrice, float mobilePrice) {
		System.out.println(mobileDiscountOfferPrice + " " + mobilePrice);
	}
}

public class Example08 {
	public static void main(String[] args) {
		Mobile refMobile = new Mobile();
		System.out.println(refMobile.getFeatures("Apple"));
		System.out.println(refMobile.getFeatures(10, "SG"));
		refMobile.getFeatures(12, 100);
	}
}
