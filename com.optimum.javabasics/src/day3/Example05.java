package day3;

// concept of local/global var

class User{
	int userID = 1000; // global var
	String userPassword = "user123"; // global var
	
	void getDetails(int userID, String password) { // local var - 50 and newuser123
		this.userID = userID;
		this.userPassword = password;
	}
	
	void showDetails() { // global var
		System.out.println(userID + " " + userPassword);
	}
}

public class Example05 {
	public static void main(String[] args) {
		User refUser = new User();
		refUser.getDetails(50, "newuser123");
		refUser.showDetails();
	}
}
