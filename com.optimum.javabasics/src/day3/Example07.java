package day3;

// method overloading ex
 
public class Example07 {

	public static void main(String[] args) {
		main("hello from line 8");
	}
	
	public static void main(String data) {
		main(10);
		System.out.println(data);
	}
	
	public static void main(int number) {
		System.out.println(main("hello from line 20", 500));
		System.out.println(number);
	}
	
	public static String main(String data, int number) {
		return data + " " + number;
	}

}
