package day3;

class UserInformation{
	void getUserDetails(Object data) {
		System.out.println(data);
	}
}

class Person{
	String personName = "ABC";
	
	@Override
	public String toString() {
		return personName;
	}
}

public class Example09 {

	public static void main(String[] args) {
		int userID = 100;
		String userName = "Jerico";
		float userCode = 100.01f;
		
		// call get userDetails() and pass values
		
		UserInformation refUser = new UserInformation();
		refUser.getUserDetails(userID);
		refUser.getUserDetails(userName);
		refUser.getUserDetails(userCode);
		
		Person refPer = new Person();
		refUser.getUserDetails(refPer);
	}

}
