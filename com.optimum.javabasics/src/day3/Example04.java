package day3;

// static - no need to create obj, can access static var or methond directly by using class name
//        - ClassName.varName or ClassName.method()
// non-static - need to create obj to access non-static var or method
// data member = var + method

// NS can access S+NS
// S can access S

class Customer {
	static int customerID = 100;
	String customerName = "James";
	
	void getDetails1() { // non-stat
		System.out.println(customerID);
		System.out.println(customerName);
	}
	
	static void getDetails2() { // stat
		System.out.println(customerID);
		//System.out.println(customerName); // error, need create obj
		Customer refCustomer = new Customer();
		System.out.println(refCustomer.customerName);
	}
	
public class Example04 {
	public void main(String[] args) {
		Customer refCustomer = new Customer();
		refCustomer.getDetails1();
		Customer.getDetails2();
	}
}
}
