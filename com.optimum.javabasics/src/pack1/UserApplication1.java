package pack1;

public class UserApplication1 {
	public static void main(String[] args) {
		
		User refUser = new User();
		refUser.getData1();
		
		// refUser.getData2() 
		// CE, cant access priv methods/var outside the class
		
		refUser.getData3();
		refUser.getData4();
	}
}
