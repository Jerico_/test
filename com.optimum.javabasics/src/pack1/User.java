package pack1;

// access mod ex

public class User {
	public void getData1() {
		System.out.println("I'm in public");
	}
	
	private void getData2() {
		System.out.println("I'm in private");
	}
	
	protected static void getData3() {
		System.out.println("I'm in protected");
	}
	
	void getData4() {
		System.out.println("I'm in default");
	}
}
