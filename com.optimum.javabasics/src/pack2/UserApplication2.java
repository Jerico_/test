package pack2;

import pack1.User;

public class UserApplication2 extends User{
	public static void main(String[] args) {
		User refUser = new User();
		refUser.getData1(); // public can access anywhere
		
		// refUser.getData2(); 
		// CE, cant access priv methods/var outside the class & package
		
		getData3();
		// prot, must extend User
		// change prot to prot stat --> no CE, to access outside package
		
		// refUser.getData4();
		// def method cant call outside package
	}
}
