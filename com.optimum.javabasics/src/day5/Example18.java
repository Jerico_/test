package day5;

abstract class Microsoft{
	abstract void getMicrosoftDriver();
}

abstract class Amazon{
	abstract void getAmazonDriver();
}

abstract class Google{
	abstract void getGoogleDriver();
}

class OptimumApplication{
	// how to override all abstract methods from abstract classes 
	Microsoft refMicrosoft = new Microsoft() {
	
	@Override
	void getMicrosoftDriver() {
		System.out.println("implement Microsoft");
	}
	};
	
	Amazon refAmazon = new Amazon() {
		
		@Override
		void getAmazonDriver() {
			System.out.println("implement Amazon");
		}
		};
		
	Google refGoogle = new Google() {
			
		@Override
		void getGoogleDriver() {
			System.out.println("implement Google");
		}
		};
}

public class Example18 {
	public static void main(String[] args) {
		OptimumApplication refOptimumApplication = new OptimumApplication();
		refOptimumApplication.refMicrosoft.getMicrosoftDriver();
		refOptimumApplication.refAmazon.getAmazonDriver();
		refOptimumApplication.refGoogle.getGoogleDriver();
	}
}
