package day5;

// co-var return type

class Company{
	Company refCompany = null;
	Company getMethod() {
		refCompany = new Company();
		return refCompany;
	}
	
	public int getNumber() {
		return 10;
	}
}

class Department extends Company{
	Department refDepartment = null;
	@Override
	public Department getMethod() {
		refDepartment = new Department();
		return refDepartment;
		
	// override parent-child rs, method name & para & return type same
	}
	
	@Override
	public int getNumber() {
		return 20;
	}
}

public class Example16 {
	public static void main(String[] args) {
		
	}
}
