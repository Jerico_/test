package day5;

// can we write a priv const? Y, for writing a singleton class/security purposes
// other users cannot create obj and slow down app

class Driver{
	private static Driver refDriver = null; // null = not pointing to any mem add
	
	private Driver(){
		System.out.println("calling Driver const");
	}
	
	public static Driver getMethod(){ // Driver data type
		if (refDriver == null) {
			refDriver = new Driver();
		}
		return refDriver; 
	}
}

public class Example15 {
	public static void main(String[] args) {
		// cant create new Driver() obj since Driver const is priv
		Driver.getMethod();
		// calling Driver.getMethod() a 2nd time has no effect, Driver not null anymore
	}
}
