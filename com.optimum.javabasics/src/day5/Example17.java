package day5;

// abstract class example
// must have at least 1 abstract method

abstract class KIOSK{
	void payBill1() { // concrete method with body
		
	}
	
	abstract void payBill2(); // abstract method cannot have body	
}

class Starhub extends KIOSK{ 
	// error at first, since KIOSK has an abstract method
	// need to override the abstract method

	@Override
	void payBill2() {
		System.out.println("pay Starhub bill");
	}	
}

class Singtel extends KIOSK{

	@Override
	void payBill2() {
		System.out.println("pay Singtel bill");
	}
}

public class Example17 {

	public static void main(String[] args) {

	}
}
