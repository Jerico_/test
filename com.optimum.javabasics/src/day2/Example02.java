package day2;

class Employee {	
	static int employeeID = 100;
	String employeeAddress = "CBP";
	
	static void employeeDetails1() {
		System.out.println(employeeID);
	}
	
	void employeeDetails2() {
		System.out.println(employeeID);
		System.out.println(employeeAddress);
	}
}

public class Example02 {
	static String data1 = "hello-1";
	static String data2 = "hello-2";
	public static void main(String[] args) {
		System.out.println(data1);
		System.out.println(data2);
		Employee.employeeDetails1();// in a class, you can only access the static variables
		Employee refEmployee = new Employee(); //Create an object to overcome the problem 
		refEmployee.employeeDetails2();
	}
}