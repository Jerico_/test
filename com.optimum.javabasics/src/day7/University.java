package day7;

public class University {
	Department refDepartment;
	
	University(Department refDepartment){
		this.refDepartment = refDepartment;
	}
	
	@Override
	public String toString() {
		return refDepartment.toString();
	}
}
