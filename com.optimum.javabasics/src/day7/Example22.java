package day7;

// final keyword example
// interface cant be final, class/method can
// cant extend a base final class (form of restriction)
// cant override a final method
// cant modify a final variable

final class UserAccessPermission{
	
}

class Mobile2{
	final void application() {
		
	}
}

class subMobile extends Mobile2{
	
}

class Laptop{
	final int number = 99; // acts like a const
//	void getNumber(int value) {
//		number = value;
//	}
}

public class Example22 {
	public static void main(String[] args) {
		
	}
}
