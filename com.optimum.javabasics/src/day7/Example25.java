package day7;

public class Example25 {
	public static void main(String[] args) {
		Student refStudent = new Student("jerico");
		Department refDepartment = new Department(refStudent);
		University refUniversity = new University(refDepartment);
		System.out.println(refUniversity);
	}
}
