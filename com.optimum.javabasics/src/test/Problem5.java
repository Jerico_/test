package test;

public class Problem5 {
	
	static void reverseNumberPattern(int rows) {	// rows --> number of rows that you want
		for (int i=rows; i>=1; i--) {				// i --> iterates for each row
			for (int j=1; j<=i; j++) {				// j --> iterates how many numbers to show per row
				System.out.print(j + " ");
			}
			System.out.println();					// start on a new line after each j iteration
		}
	}
	
	public static void main(String[] args) {

		reverseNumberPattern(5);
	}

}
