package test;

public class Product {
	private int productID;
	private String productName;
	private double price;
	private int quantity;
	private double totalPrice;
	
	public Product(int productID,  String productName, double price, int quantity, double totalPrice) {
		super();
		this.productID = productID;
		this.productName = productName;
		this.price = price;
		this.quantity = quantity;
		this.totalPrice = totalPrice;
	}
	
	public int getProductID() {
		return productID;
	}
	
	public String getProductName() {
		return productName;
	}
	
	public double getPrice() {
		return price;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public double getTotalPrice() {
		return totalPrice;
	}
}
