package test;

public class Problem3 {
	
	static void letterPattern(int rows) {				// rows --> number of rows that you want
		for (int i=1; i<=rows; i++) {					// i --> iterates for each row
			for (int j=1; j<=i; j++) {					// j --> iterates how many letters to show per row
				System.out.print((char)(i+64) + " ");	// using Base64 ASCII to print the letters iteratively, where A=65
			}
			System.out.println();						// start on a new line after each j iteration
		}
	}
	
	public static void main(String[] args) {
		
		letterPattern(5);
	}
}
