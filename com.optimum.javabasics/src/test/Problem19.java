package test;

public class Problem19 {
	
	static void whileLoopNumberPalindrome(int num) {					// num --> number to be checked whether it's a palindrome or not
	    int reversedNum=0, remainder;									// initialise variables to be used
	    int originalNum = num;											// temporarily store num here 

	    while (num!=0) {
	      remainder = num % 10;											// remainder --> store the tens place of the number (eg store 1 for 121)
	      reversedNum = reversedNum * 10 + remainder;					// reversedNum --> store value of current reversedNum + remainder
	      num /= 10;													// num --> store the number without the tens place (eg store 12 for 121)
	    }	
	    
	    if (originalNum==reversedNum) {									// if the final reversedNum is the same as the originalNum
	    	System.out.println(originalNum + " is a palindrome");		
	    }
	    
	    else {															// if the final reversedNum is not the same as the originalNum
	    	System.out.println(originalNum + " is not a palindrome");
	    }	
	}
	
	static void forLoopNumberPalindrome(int num) {						// num --> number to be checked whether it's a palindrome or not
	     int reversedNum=0, remainder; 									// initialise variables to be used
	     int originalNum = num;											// temporarily store num here 
	     
	     for(; num!=0; num/=10) {										// num --> for loop to store the number without the tens place (eg store 12 for 121 for first iteration)
	    	 remainder = num % 10; 										// remainder --> store the tens place of the number (eg store 1 for 3551)
	         reversedNum = reversedNum * 10 + remainder;				// reversedNum --> store value of current reversedNum + remainder
	     } 
	     
	     if (originalNum==reversedNum) {								// if the final reversedNum is the same as the originalNum
		     System.out.println(originalNum + " is a palindrome");
	     }
	     
	     else {															// if the final reversedNum is not the same as the originalNum
	         System.out.println(originalNum + " is not a palindrome");
	     }	   
	}
	
	public static void main(String[] args) {
		
		whileLoopNumberPalindrome(121);
		forLoopNumberPalindrome(3551);
	}

}
