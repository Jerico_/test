package test;

import java.util.HashSet;
import java.util.Set;

public class Problem12 {
	static void findSecondSmallestAndSecondLargest(int[] array) {							// array --> given array to be checked

		Set<Integer> set = new HashSet<Integer>();											// creating a HashSet to store unique elements from the array
		for (int element:array) {															// checks each element inside array
	    	set.add(element);																// adds the unique element to our set
		}
		
		Integer[] array2 = set.toArray(new Integer[set.size()]);							// converting the set to an array so we can access the elements by index
		System.out.println("Second smallest element in array: " + array2[1]);				// index 1 for the second smallest element
		System.out.println("Second largest element in array: " + array2[array2.length-2]);	// index array2.length-2 for the second largest element
	}
	
	public static void main(String[] args) {
		int[] array = {1,5,4,2,3,6};
		findSecondSmallestAndSecondLargest(array);
	}

}
