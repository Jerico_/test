package test;

public class Problem6 {
	
	static void evenStarPattern(int maxStars) {	// maxStars --> number of rows less than or equal to max number of stars that you want 
												// eg maxStars = 5 will show from 1 star up to 4 stars (since 5 is odd)
		
		for (int i=1; i<=maxStars; i++) {		// i --> iterates for each row				
			if (i==1 || i%2==0) {				// we want i==1 (base case) or i%2==0 (even cases)
				for (int j=1; j<=i; j++) {		// j --> iterates how many *s per row
					System.out.print("*");
				}
			} else {
				continue;						// if i is odd, we continue with the iteration (don't print any *s)
			}
			System.out.println();				// start on a new line after each j iteration
		}
	}
	
	public static void main(String[] args) {

		evenStarPattern(6);
	}

}
