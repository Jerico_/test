package test;

import java.util.Scanner;

public class Problem16 {
	
	static void forLoopFibonacci(int terms) {							// terms --> how many terms in the Fibonacci sequence do you want
		int num1=0; int num2=1;											// initialise the first 2 terms, 0 and 1
		for (int i=1; i<=terms; i++) {									// i --> iterates for each term
            System.out.print(num1 + " ");								// prints num1
            int sumOfPreviousTwo = num1 + num2;							// sumOfPreviousTwo --> temporarily store the value of num1 + num2
            num1 = num2;												// store value of num2 to num1
            num2 = sumOfPreviousTwo;									// store value of sumOfPreviousTwo to num2
        }
	}
	
	static void whileLoopFibonacci(int terms) {							// terms --> how many terms in the Fibonacci sequence do you want
		int num1=0; int num2=1;											// initialise the first 2 terms, 0 and 1
		int i=1;
        while (i<=terms) {												// i --> iterates for each term
            System.out.print(num1 + " ");								// prints num1
            int sumOfPreviousTwo = num1 + num2;							// sumOfPreviousTwo --> temporarily store the value of num1 + num2
            num1 = num2;												// store value of num2 to num1
            num2 = sumOfPreviousTwo;									// store value of sumOfPreviousTwo to num2
            i++;														// increase i to continue the while loop
        }
	}
	
	static void givenNumberFibonacci() {
		int terms, num1=0, num2=1;										// initialise the first 2 terms, 0 and 1
        System.out.println("How may numbers in the sequence?: ");
        Scanner scanner = new Scanner(System.in);
        terms = scanner.nextInt();										// terms --> how many terms in the Fibonacci sequence do you want
        scanner.close();
        System.out.print("Fibonacci Series of "+ terms +" numbers: ");

        int i=1;
        while(i<=terms) {												// i --> iterates for each term
            System.out.print(num1+" ");									// prints num1
            int sumOfPreviousTwo = num1 + num2;							// sumOfPreviousTwo --> temporarily store the value of num1 + num2
            num1 = num2;												// store value of num2 to num1
            num2 = sumOfPreviousTwo;									// store value of sumOfPreviousTwo to num2
            i++;														// increase i to continue the while loop
        }			
	}
	public static void main(String[] args) {
			
		forLoopFibonacci(4);
		System.out.println();
		whileLoopFibonacci(4);
		System.out.println();
		givenNumberFibonacci();
	}

}
