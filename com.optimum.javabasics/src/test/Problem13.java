package test;

import java.util.HashSet;
import java.util.Set;

public class Problem13 {
	static void lengthAfterRemovingDuplicates(int[] array) {						// array --> given array to be checked
		
		Set<Integer> set = new HashSet<Integer>();									// creating a HashSet to store unique elements from the array
		for (int element:array) {													// checks each element inside array
	    	set.add(element);														// adds the unique element to our set
		}
		System.out.println("Length of array without duplicates: " + set.size());	// prints out the size of the set without the duplicate elements
	}
	public static void main(String[] args) {
		int[] array = {1,2,3,1,2,3,4};
		lengthAfterRemovingDuplicates(array);
	}

}
