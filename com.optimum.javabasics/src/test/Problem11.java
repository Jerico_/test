package test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Problem11 {
	static void removeDuplicates(int[] array) {		// array --> given array to be checked

		Set<Integer> set = new HashSet<Integer>();	// creating a HashSet to store unique elements from the array
		for (int element:array) {					// checks each element inside array
	    	set.add(element);						// adds the unique element to our set
		}
		System.out.println(set);					// prints out the set without the duplicate elements
		
	}
	
	public static void main(String[] args) {
		int array[] = {1,2,3,1,2,3,4};
		removeDuplicates(array);
		
//		using Stream
		Integer[] myArray = new Integer[] {1,2,3,1,2,3,4};
		System.out.println((List<Integer>) Arrays.asList(myArray).stream().distinct().collect(Collectors.toList()));
	}

}
