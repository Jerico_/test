package test;

public class Problem10 {
	
	static void findCommonElements(int[] array1, int[] array2, int[] array3) {	// input 3 arrays to be compared
		
		int i=0, j=0, k=0;														// initialising iterators to be used for while loop
																				// i,j,k for arrays 1,2,3 respectively
		
        while (i<array1.length && j<array2.length && k<array3.length){			// while loop as long as i,j,k are all less than their respective array lengths
        	if (array1[i]==array2[j] && array2[j]==array3[k]){   				// if there is a common element in all 3 arrays
            	 System.out.print(array1[i]+" ");   							// print that element
            	 i++; j++; k++; 												// move on to the next index of each array
            }
        	else if (array1[i]<array2[j])										// if element in array1 is less than the element in array2
                 i++;															// move on to the next index of array1
            else if (array2[j]<array3[k])										// if element in array2 is less than the element in array3
                 j++;															// move on to the next index of array2
            else																// if element in array3 is less than the element in array1
                 k++;															// move on to the next index of array3
        }      
        
	}
	
	public static void main(String[] args) {
		int array1[] = {1,5,10,20,40,80};
		int array2[] = {6,7,20,80,100};
		int array3[] = {3,4,15,20,30,70,80,120};
		
		findCommonElements(array1, array2, array3);
		
	}

}
