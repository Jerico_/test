package test;

public class Problem1 {
	
	static void starPattern(int rows){	// rows --> number of rows that you want
		for (int i=1; i<=rows; i++) {	// i --> iterates for each row
			for (int j=1; j<=i; j++) {	// j --> iterates how many *s per row
				System.out.print("*");
			}
			System.out.println();		// start on a new line after each j iteration
		}
	}
	
	public static void main(String[] args) {
		
		starPattern(7);
	}
}
