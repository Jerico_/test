package test;

import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class Problem15 {
	public static void main(String[] args) {
		int productID = 0;
		String productName = null;
		int quantity = 0;
		double price = 0.0;
		double totalPrice = 0.0;
		double finalPrice = 0.0;
	
		String userChoice = null;
		Scanner scan = new Scanner(System.in);
		List<Product> product = new ArrayList<Product>();
		
		do {		
		System.out.print("Enter Product ID: ");
		productID = scan.nextInt();
		scan.nextLine();
		
		System.out.print("Product Name: ");	
		productName = scan.nextLine();
		
		System.out.print("Price in SGD: ");	
		price = scan.nextDouble();
		
		System.out.print("Quantity: ");
		quantity = scan.nextInt();
		
		totalPrice = price*quantity;
		finalPrice += totalPrice;
		
		product.add(new Product(productID, productName, price, quantity, totalPrice));
		
		System.out.print("Wish to Continue? (Y/N): ");
		userChoice = scan.next();		
		} while (!userChoice.equals("N"));
		
		System.out.println("\nYour product list as follows: \n");
		System.out.format("%-15.15s %-15.15s %-15.15s %-15.15s\n","Product ID","Product Name","Price","Quantity");
		
		for (Product p : product) {
			System.out.format("%-15.15s %-15.15s %-15.2f %-15s\n", p.getProductID(), p.getProductName(), p.getPrice(), p.getQuantity());
		}

		System.out.format("\n%-15.15s %-15.2f", "Total Price", (double) finalPrice);
		System.out.format("\n%-15.15s %-15.15s", "Flat Discount", "20%");
		System.out.format("\n%-15.15s %-15.2f", "Discount Amount", (double) finalPrice*0.2);
		System.out.format("\n%-15.15s %-15.2f", "Amount to Pay", (double) finalPrice*0.8);
		scan.close();
	}
}