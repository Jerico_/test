package test;

public class Problem7 {
	
	static void numberPattern2(int rows) {		// rows --> number of rows that you want
		for (int i=1; i<=rows; i++) {			// i --> iterates for each row
            for (int j=i; j<=(2*i-1); j++) {	// j --> iterates from i up to the highest number per row
                System.out.print(j + " ");
            }
            for (int k=(2*i-2); k>=i; k--) {	// k --> iterates from the highest number down to i per row
                System.out.print(k + " ");
            }
            System.out.println();				// start on a new line after each j and k iteration
		}
	}
	
	public static void main(String[] args) {
		
		numberPattern2(5);
	}

}
