package test;

public class Problem18 {
	
	static void armstrong(int num) {									// num --> number to be checked whether it's an Armstrong number or not
		int originalNumber, remainder, result=0;						// initialise variables to be used
	    originalNumber = num;											// temporarily store num here 

        while (originalNumber!=0) {
            remainder = originalNumber % 10;							// remainder --> store the tens place of the number (eg store 3 for 153)
            result += Math.pow(remainder, 3);							// result --> store value of current result + remainder cubed
            originalNumber /= 10;										// originalNumber --> store the number without the tens place (eg store 15 for 153)
        }       
        
	    if (result==num) {												// if the final result is the same as the input number num
		     System.out.println(num + " is an Armstrong number");
	    }
	    else {															// if the final result is not the same as the input number num
	         System.out.println(num + " is not an Armstrong number");
	    }	
	}
	
	public static void main(String[] args) {
		armstrong(153);
	}

}
