package test;

import java.util.ArrayList;
import java.util.Scanner;

public class Problem14 {
	
	public static void main(String[] args) {
		String emailAddress = null;
		String password = null;
		String securityKey = null;
		double balance = 0.0;
		
		ArrayList<User> userList = new ArrayList<>();
		User newUser = new User(emailAddress, password, securityKey, balance);
		User defaultUser = new User("xyz", "xyz", "xyz", 1);
		userList.add(defaultUser);
		
		String userChoice = null;
		String userChoice2 = null;
		Scanner scan = new Scanner(System.in);
		
		do {
			System.out.println("User Home Page:\n");
			System.out.println("1. Register");
			System.out.println("2. Login");
			System.out.println("3. Forgot Password");
			System.out.println("4. Logout\n");
			System.out.print("Enter your choice: ");
			int option = scan.nextInt();
			scan.nextLine();
			
			switch (option) {
			case 1:
				System.out.print("Enter email address: ");
				emailAddress = scan.next();
				
				while (emailAddress.equals(newUser.getEmailAddress())) {
					System.out.println("Email already exists!");
					System.out.print("Enter email address: ");
					emailAddress = scan.next();
				}
				
				System.out.print("Enter password: ");
				password = scan.next();
				
				System.out.print("Retype password: ");
				String retypedPassword = scan.next();
				
				while (!retypedPassword.equals(password)) {
					System.out.println("Password doesn't match!");
					System.out.print("Retype password: ");
					retypedPassword = scan.next();
				}

				System.out.print("What's your favourite colour?");
				securityKey = scan.next();
				System.out.println(securityKey + " is your security key, in case you forget your password.");						 
				
				newUser = new User(emailAddress, password, securityKey, balance);
				userList.add(newUser);
	
				System.out.println("Registration successful!");
				break;

			case 2:
				System.out.print("Enter User ID: ");
				String emailAddress2 = scan.next();
				
				System.out.print("Enter password: ");
				String password2 = scan.next();
				
				for (User u : userList) {
					newUser = u;

					if (emailAddress2.equals(newUser.getEmailAddress()) && password2.equals(newUser.getPassword())) {
						System.out.println("Login successful!\n");
						
						do {
							System.out.println("1. Check Available Bank Balance");
							System.out.println("2. Deposit Amount");
							System.out.println("3. Withdraw Amount");
							
							System.out.print("Enter your choice: ");
							int option2 = scan.nextInt();
							
							switch (option2) {
							case 1:
								System.out.println("Available balance: " + newUser.getBalance());
								System.out.print("Wish to continue? (Y/N): ");
								userChoice2 = scan.next();
								break;
	
							case 2:
								System.out.print("Enter amount to deposit: ");
								double deposit = scan.nextDouble();
	
								if (deposit <= 0) {
									System.out.println("Cannot deposit zero or a negative value");
								} else {
									newUser.deposit(deposit);
									System.out.println("$" + deposit + " deposited.");
								}
								
								System.out.print("Wish to continue? (Y/N): ");
								userChoice2 = scan.next();
								break;
							
							case 3:
								System.out.print("Enter amount to withdraw: ");
								double withdraw = scan.nextDouble();
	
								if (withdraw > newUser.getBalance() || withdraw <= 0) {
									System.out.println("Invalid amount to withdraw.");
								} else {
									newUser.withdraw(withdraw);
									System.out.println("$" + withdraw + " withdrawn.");
								}
								
								System.out.print("Wish to continue? (Y/N): ");
								userChoice2 = scan.next();
								break;
								
							default:
								System.out.println("Option not available!");
								break;
							}

						} while (!userChoice2.equals("N"));
					}
				
					else {
						System.out.println("Invalid user ID or password"); 
					}			
				}
				break;
				
			case 3:
				System.out.print("Enter User ID: ");
				String emailAddress3 = scan.next();
				
				System.out.print("Enter security key: ");
				String securityKey3 = scan.next();
				
				if (emailAddress3.equals(newUser.getEmailAddress()) && securityKey3.equals(newUser.getSecurityKey())) {
					System.out.print("Enter new password: ");
					password = scan.next();
					newUser.setPassword(password);
					
					System.out.print("Retype password: ");
					retypedPassword = scan.next();
					
					while (!retypedPassword.equals(password)) {
						System.out.println("Password doesn't match!");
						System.out.print("Retype password: ");
						retypedPassword = scan.next();
					}
					
					System.out.print("What's your favourite colour?");
					securityKey = scan.next();
					System.out.println(securityKey + " is your security key, in case you forget your password.");	
					
				} else {
					System.out.println("Invalid user ID or security key");
				}
			
				System.out.println("Password reset successfully!");
				break;
			
			case 4:
				System.out.println("Logout successfully!");
				break;
			
			default:
				System.out.println("Option not available!");
				break;
			}

			System.out.print("Go back to main menu? (Y/N): ");
			userChoice = scan.next();
			
		} while (!userChoice.equals("N"));
		
		System.out.println("Thanks for banking with us!");
		scan.close();
	}
}