package test;

import java.math.BigInteger;

public class Problem17 {
	
	static void forLoopFactorial(int num) {							// num --> factorial of which number do you want
		long factorial=1;											// factorial --> store the total value, starting with 1
	    for(int i=1; i<=num; i++) {									// i --> iterate until the number that you want
	    	factorial *= i;											// store current value into factorial
	    }
	    System.out.println(factorial);
	}
	
	static void bigIntegerFactorial(int num) {						// num --> factorial of which number do you want
		BigInteger factorial = new BigInteger("1");					// factorial --> store the total value, starting with 1
		for (int i=2; i<=num; i++) {								// i --> iterate until the number that you want
			factorial = factorial.multiply(BigInteger.valueOf(i));	// store current value into factorial using multiply() function of BigInteger
		}
		System.out.println(factorial);
	}
	
	static void whileLoopFactorial(int num) {						// num --> factorial of which number do you want
		long factorial=1;											// factorial --> store the total value, starting with 1
		int i=1;
		while(i<=num) {												// i --> iterate until the number that you want
			factorial *= i;											// store current value into factorial	
			i++;													// increase i to continue the while loop
		}
		System.out.println(factorial);
	}
	
	static int recursionFactorial(int num) {						// num --> factorial of which number do you want

		if (num==0) {												// base case, 0! = 1
			return 1;    
		}

		else 
			return num *= recursionFactorial(num-1);				// invoke RecursionFactorial() again, this time lowering the number by 1
	}
	
	public static void main(String[] args) {

		forLoopFactorial(5);
		bigIntegerFactorial(5);
		whileLoopFactorial(5);
		System.out.println(recursionFactorial(5));

	}

}
