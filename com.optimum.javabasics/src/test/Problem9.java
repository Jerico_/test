package test;

import java.util.stream.IntStream;

public class Problem9 {
	
	static void valueInArray(int[] myArray, int myValue) {					// myArray --> given array to be checked
																			// myValue --> value to be checked
		
		for (int element : myArray) {										// checks each element inside myArray
			if (element == myValue) {										// if the current element is the same as myValue 
				System.out.println(myValue + " is in the given array");
				break;														// stops the for loop
			}
			
			else {
				System.out.println(myValue + " is not in the given array");	// if the current element is not the same as myValue
				break;														// stops the for loop
			}
		}
	}
	
	public static void main(String[] args) {
		
		int[] myArray = {5, 7, 2, 3, 10};
		valueInArray(myArray, 5);
		
//		using IntStream
		System.out.println(IntStream.of(myArray).anyMatch(x -> x == 5));	// true if myValue = 5 is in the given array
	}

}
