package test;

public class User {
	private String emailAddress;
	private String password;
	private String securityKey;
	private double balance;
	
	public User(String emailAddress, String password, String securityKey, double balance) {
		super();
		this.emailAddress = emailAddress;
		this.password = password;
		this.securityKey = securityKey;
		this.balance = balance;
	}
	
	public void deposit(double amount) {
        balance += amount;
    }

    public double withdraw(double amount) {
        balance -= amount;
        return amount;
    }

	public String getEmailAddress() {
		return emailAddress;
	}
	
	public String getPassword() {
		return password;
	}
	
	public String getSecurityKey() {
		return securityKey;
	}
	
	public double getBalance() {
		return balance;
	}
	
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public void setSecurityKey(String securityKey) {
		this.securityKey = securityKey;
	}
	
	public void setBalance(double balance) {
		this.balance = balance;
	}
}