package test;

public class Problem20 {
	
	static void usingReverseStringPalindrome(String str) {				// str --> String to be checked whether it's a palindrome or not
	      String data = new StringBuffer(str).reverse().toString();		// data --> initialise an object StringBuffer, taking in the String str
	      																// reverse() method to reverse the characters of the string
	      																// toString() method to represent the object StringBuffer back to a string
	      
	      if (str.equals(data)) {										// if original str is the same as data
	         System.out.println(str + " is a palindrome");
	      }
	      else {
	         System.out.println(str + " is not a palindrome");			// if original str is not the same as data
	      }	      
	}
	
	static void withoutUsingReverseStringPalindrome(String str) {		// str --> String to be checked whether it's a palindrome or not
		int i;															// initialise i for the for loop
	    int start=0;													// start --> index at the start of the string
	    int end = str.length()-1;										// end --> index at the end of the string
	    int middle = (start+end)/2;										// middle --> index in the middle of the string
	 
	    for (i=start; i<=middle; i++) {									// for loop until the middle
	      if (str.charAt(start)==str.charAt(end)) {						// if the characters at the start and end of the string are the same
	    	  start++;													// increase the index of start by 1, getting closer to the middle
	    	  end--;													// decrease the index of end by 1, getting closer to the middle
	      }
	      else															// if the characters at the start and end of the string are not the same
	    	  break;													// break out of the loop
	    }
	    if (i == middle+1)												// if we have reached the middle of the string and they are the same
	      System.out.println(str + " is a palindrome");
	    else															// if we have reached the middle of the string and they are not the same
	      System.out.println(str + " is not a palindrome");
	}
	public static void main(String[] args) {

		usingReverseStringPalindrome("abba");
		withoutUsingReverseStringPalindrome("abc");
	}

}
