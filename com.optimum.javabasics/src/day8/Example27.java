package day8;

// Type Casting

// Prim Data Types
// byte short char int long float double boolean

// Wrapper Classes (java coll framework dont accept prim data type, only classes/wrapper classes)
// Byte Short Character Integer Long Float Double Boolean

class CastingDemo{
	void getData() {
		int number1 = 100; 
		long number2 = number1; // implicit casting done by JVM
		
		float number3 = 50;
		double number4 = number3;
	}
	
	void getData2() {
		long number5 = 10;
		int number6 = (int) number5; // explicit type casting
		
		double number7 = 30;
		float number8 = (float) number7;
	}
	
	void getData3() {
		String info1 = "hi";
		Object info2 = info1; // UpCasting done by JVM
		
		Object info3 = "hello";
		String info4 = (String) info3; // DownCasting
	}
	
	void getData4() {
		int number9 = 1000;
		Integer refInteger = number9; // AutoBoxing done by JVM
	}
	
	void getData5() {
		Double refDouble = 555.123;
		double number10 = refDouble; // UnBoxing done by JVM
	}
}

public class Example27 {
	public static void main(String[] args) {
		//System.out.println();
	}
}
