package day8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

// how to take user input using BufferedReader Class
// BufferedReader forces us to throw exception, unlike Scanner

class BufferedReaderDemo{
	void getName() throws IOException {
		
		BufferedReader refBufferedReader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter name: ");
		
		String name1 = refBufferedReader.readLine();
		System.out.println(name1);
		
		System.out.println("Enter a character: ");
		
//		char charData = (char) refBufferedReader.read();  
		char charData = (char) refBufferedReader.readLine().charAt(1);
		System.out.println(charData);
	}
}

public class Example28 {
	public static void main(String[] args) throws IOException {
		new BufferedReaderDemo().getName();
	}
}
