package day1;
//
//public class Example01 {
//
//	public static void main() {
//		
//		//System.out.println("hello world..");
//	
//	}
//
//}

import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;    

public class Example01 {
  public static void main(String[] args) {    
   DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
   LocalDateTime now = LocalDateTime.now();  
   System.out.println(dtf.format(now));  
  }    
}    