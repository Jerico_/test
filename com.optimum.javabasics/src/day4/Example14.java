package day4;

// Method Overriding or dynamic polymorphism
// method name + para MUST be same
// MUST have parent-child rs

class Laptop{
	void getDetails1(){
		System.out.println("I am in Laptop - getDetails1()");
	}
	
	static void getDetails2(){
		System.out.println("I am in Laptop - getDetails2()");
	}
}

class OmenHP extends Laptop{
	@Override
	void getDetails1(){
		System.out.println("I am in OmenHP - getDetails1()");
	}
	
	//@Override, derived class cannot override base class static method
	static void getDetails2(){
		System.out.println("I am in OmenHP - getDetails2()");
	}
}

public class Example14 {
	public static void main(String[] args) {
//		Laptop refLaptop = new OmenHP();
//		refLaptop.getDetails1();
//		refLaptop.getDetails2();
//		OmenHP.getDetails2();
		
//		OmenHP refOmenHP = (OmenHP) new Laptop(); // error, use typecasting
		OmenHP refOmenHP = new OmenHP();
		refOmenHP.getDetails1();
		//refOmenHP.getDetails2(); // ClassCastException
		// creating obj of parent and referring to child then calling child's method
	}
}
