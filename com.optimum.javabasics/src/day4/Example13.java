package day4;

// Inheritance
// java supports multilevel inh for class level
// java supports multiple inh through interface
// use super if method/var name is the same

class Programming{ // super/base/parent class 
	void showProgramming(){
		System.out.println("I am in Programming class");
	}
}

class Java extends Programming{ // sub/derived/child class
	void showJava(){
		System.out.println("I am in Java class");
	}
}

class Kotlin extends Java{
	void showKotlin(){
		System.out.println("I am in Kotlin class");
	}
}

class KotlinSecurity extends Kotlin{
	void getDetails(){
		showProgramming();
		showJava();
		showKotlin();
	}
}

class MyClass extends KotlinSecurity{
	void getDetails(){
		System.out.println("data from MyClass");
	}
	
	void getMyClass(){
		this.getDetails(); // calls line 34
		super.getDetails(); // calls line 26
	}
}

public class Example13 {
	public static void main(String[] args) {
		new MyClass().getMyClass(); // calling line 38
	}
}
