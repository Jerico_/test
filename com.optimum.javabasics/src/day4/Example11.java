package day4; 

// constructor concept
// JVM creates default obj based on name of new obj
// const cannot have return
// can call const using obj
// cannot call const using ref --> compilation error

class User{
	int number;
	
	User(){ // default const
		this(10, "Jerico");
		number = 10;
		System.out.println(number);
		
	}
	
	User(int number, String name){
		this(true, 99999);
		System.out.println(number + " " + name);
	}
	
	User(boolean data, long digit){
		System.out.println(data + " " + digit);
	}
}

public class Example11 {
	public static void main(String[] args) {
		new User();
		
//		User userRef = new User();
//		userRef.User(10, "Jerico"); // comp error
	}
}
