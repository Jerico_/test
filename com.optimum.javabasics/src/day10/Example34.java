package day10;

class OuterA{
	void methodOfOuterA() { // method local inner class
		int num = 10;
		class InnerB{
			int num = 20;
			void methodOfInnerB() {
				System.out.println(num); // 10 or 20
			}
		}
		InnerB inner = new InnerB();
		inner.methodOfInnerB();
		System.out.println(num);
	}
}

public class Example34 {
	public static void main(String[] args) {
		OuterA outer = new OuterA();
		outer.methodOfOuterA();
	}
}
