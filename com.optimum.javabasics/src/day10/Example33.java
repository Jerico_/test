package day10;

import java.util.Scanner;

class OuterClassDemo{
	// NS inner class, nested/inner class for security purposes
	// priv inner class --> none of the approach will work
	// must encap method and create ref in OuterClass
	
	// Approach 3
	public void innerClassMethod() {
		refInnerClassDemo.getDetailsInnerClassDemo();
	}
	
	private class InnerClassDemo{ 
		void getDetailsInnerClassDemo() {
			System.out.println("Enter password:");
			Scanner refScanner = new Scanner(System.in);
			String password = refScanner.next();
			System.out.println(password);
		}		
	}
	
//	Approach 1 & 3
	InnerClassDemo refInnerClassDemo = new InnerClassDemo(); 
}

public class Example33 {
	public static void main(String[] args) {
		OuterClassDemo refOuterClassDemo = new OuterClassDemo();
//		Approach 1
//		refOuterClassDemo.refInnerClassDemo.getDetailsInnerClassDemo();
		
//		Approach 2
//		OuterClassDemo.InnerClassDemo refInnerClassDemo = refOuterClassDemo.new InnerClassDemo();
//		refInnerClassDemo.getDetailsInnerClassDemo();
		
//		Approach 3
		refOuterClassDemo.innerClassMethod();
	}
}
