package day10;

// String class ex
// == checks if pointing to same mem add, use for prim data type
// .equals() checks if same value, use for Class
// 34 pointing to same mem add in String Constant Pool in Heap 
// p1p2 pointing to diff obj
// p1p2 same after overriding hashCode() and equals() in Person.java
// override hashCode() --> make obj point to the same mem add
// override equals() --> check if same data

public class Example36 {
	public static void main(String[] args) {
		String ref1 = "data1";
		String ref2 = "data1";
		String ref3 = new String ("data1");
		String ref4 = new String ("data1");
		
		Person refPerson1 = new Person("data1");
		Person refPerson2 = new Person("data1");
		
		// check if ref1 ref2 same using == operator
		if (ref1 == ref2) {
			System.out.println("12 same");
		} else {
			System.out.println("12 not same");
		}
		
		// check if ref1 ref2 same using .equals() method
		if (ref1.equals(ref2)) {
			System.out.println("12 same");
		} else {
			System.out.println("12 not same");
		}
		
		// check if ref2 ref3 same using == operator
		if (ref2 == ref3) {
			System.out.println("23 same");
		} else {
			System.out.println("23 not same");
		}
		
		// check if ref2 ref3 same using .equals() method
		if (ref2.equals(ref3)) {
			System.out.println("23 same");
		} else {
			System.out.println("23 not same");
		}
		
		// check if ref3 ref4 same using == operator
		if (ref3 == ref4) {
			System.out.println("34 same");
		} else {
			System.out.println("34 not same");
		}
		
		// check if ref3 ref4 same using .equals() method
		if (ref3.equals(ref4)) {
			System.out.println("34 same");
		} else {
			System.out.println("34 not same");
		}
		
		// check if person1 person2 same using .equals() method
		if (refPerson1.equals(refPerson2)) {
			System.out.println("p1p2 same");
		} else {
			System.out.println("p1p2 not same");
		}
	}
}
