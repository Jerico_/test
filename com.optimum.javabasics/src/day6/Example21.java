package day6;

class Base{
	int number = 100; // number = 10 replaces 100. use this to get 100
	// Base(int num) will give error, must have no args. use suoer
	Base(int number){ // local var cannot be static - comp error
		System.out.println(number + " from Base");
	}
	
//	void display(static int number) { // also error
//		
//	}
}

class Derived extends Base{
	Derived(int number){
		super(number);
		System.out.println(number + " from Derived");
	}
}

class SubDerived extends Derived{
	SubDerived(int number, String data){
		super(number);
		System.out.println(number + " " + data);
	}
}
public class Example21 {
	public static void main(String[] args) {
		new SubDerived(10, "data1");
	}
}
