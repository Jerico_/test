package day6;

abstract class Airline{
	int flightID;
	String flightName;
	
	Airline(){
		flightID = 999;
		flightName = "British Airlines";
	}
	
	abstract void getFlightDetails();
}

class MyAirline extends Airline{
	@Override
	void getFlightDetails() {
		this.flightID = 123;
		this.flightName = "abc";
		System.out.println(flightID + " " + flightName);
	}
}

public class Example20 {
	public static void main(String[] args) {
		MyAirline refAirline = new MyAirline();
		refAirline.getFlightDetails();
	}
}
