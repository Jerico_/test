package day6;

abstract class Vehicle{
	abstract void withDriver();
	abstract void driverless();
}

abstract class Car extends Vehicle{
	abstract void supportsAI();
}

abstract class MyCar extends Car{
	// if abs class ext another abs class then no need override any abs methods
}

class Car1 extends MyCar{
	// we dw user to know which Vehicle class we are using
	@Override
	void driverless() {
		System.out.println("inside driverless");
	}
	
	@Override
	void supportsAI() {
		System.out.println("inside supportsAI");
	}
	
	@Override
	void withDriver() {
		System.out.println("inside withDriver");
	}
}

public class Example19 {
	public static void main(String[] args) {
		
	}
}
