package day6;

// interface example

@FunctionalInterface
interface One{
	void displayMethod1();
	// void displayMethod2(); comp error, FI will only have 1 abs method
}

abstract class OptimumCar{
	
	int number = 99; // can also use final static
	
	void display1() {
		System.out.println("disp1");
	}
	
	abstract void display2();
}

interface OptimumCar2 { 
	// interfaces are abstract classes
//	void display3() { // comp error, cannot write concrete method, use default
//		
//	}
	
	int number = 50; // final static by def
	
	default void display4() { // no comp error
		// purpose is to call all the private static methods
		display5();
		display6();
	}
	
    static void display5() { // no comp error
		System.out.println("disp5");
	}
    
    private static void display6() { // no comp error
    	System.out.println("disp6");
    }
    
    abstract void display7();
}

class MyClass extends OptimumCar implements OptimumCar2{
	// can we ext >1 abs class? N
	// can we imp >1 int? Y
	
	@Override
	void display2() { // not public, from abs
		System.out.println("disp2");
	}
	
	@Override
	public void display7() { // public by def cause from int
		System.out.println("disp7");
	}
	
	void display8() {
		display4(); // imp -> can call directly
	}
}

public class Example22 {
	public static void main(String[] args) {
		MyClass refMyClass = new MyClass();
		refMyClass.display2();
		refMyClass.display7();
		refMyClass.display8();
	}
}
