package user.service;

import java.util.Scanner;

import user.dao.UserLoginDAOAuthenticationImplementation;
import user.dao.UserLoginDAOInterface;
import user.pojo.User;

public class UserLoginServiceImplementation implements UserLoginServiceInterface{

	UserLoginDAOInterface refUserLoginDAOInterface = null;
	User refUser;
	
	@Override
	public void callUserDAOImplementation() {
		refUser = new User();
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter username: ");
		String name = sc.next();
		refUser.setUserLoginID(name); 
		
		System.out.println("Enter password: ");
		String password = sc.next();
		refUser.setPassword(password); 
		
		refUserLoginDAOInterface = new UserLoginDAOAuthenticationImplementation();
		refUserLoginDAOInterface.userLoginAuthentication(refUser); 
		
		if (refUserLoginDAOInterface.userLoginAuthentication(refUser)) {
			System.out.println("Authenticated");
		} else {
			System.out.println("Denied");
		}
		sc.close();
	}
}