package user.controller;

import user.service.UserLoginServiceImplementation;
import user.service.UserLoginServiceInterface;

public class UserLoginController {
	// declare ref as global var so other methods can access this
	UserLoginServiceInterface refUserLoginServiceInterface = null;
	
	public void getUserLoginService() {
		refUserLoginServiceInterface = new UserLoginServiceImplementation();
		refUserLoginServiceInterface.callUserDAOImplementation();
	}
}
